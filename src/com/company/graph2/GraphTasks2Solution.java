package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(startIndex, 0);
        deque.offerFirst(startIndex);
        while (!deque.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[startIndex][i] > 0) {
                    if (map.containsKey(i)) {
                        if (map.get(i) > map.get(startIndex) + adjacencyMatrix[startIndex][i]) {
                            map.replace(i, map.get(startIndex) + adjacencyMatrix[startIndex][i]);
                            if (!deque.contains(i)) deque.offerFirst(i);
                        }
                    }else{
                        map.put(i, map.get(startIndex) + adjacencyMatrix[startIndex][i]);
                        if (!deque.contains(i)) deque.offerFirst(i);
                    }
                }
            }
            if (!deque.isEmpty()) startIndex = deque.pollLast();
        }
        return map;
    }



    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int[] arr;
        int res = 0;
        int value;
        int startPoint = 0;
        Set<Integer> dots = new HashSet<>();
        dots.add(startPoint);
        while (dots.size() != adjacencyMatrix.length) {
            int u = 0;
            int min = 999999999;
            int v = 0;
            for(int dot : dots) {
                arr = adjacencyMatrix[dot];
                for(int j = 0; j < arr.length; j++) {
                    value = arr[j];
                    if ((value < min) && value != 0 && !dots.contains(j)) {
                        u = dot;
                        v= j;
                        min = value;
                    }
                }
            }
            res += min;
            dots.add(u);
            dots.add(v);
        }
        return res;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return null;
    }
}
